/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.management.Query;
//import org.apache.solr.client.solrj.*;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

/**
 *
 * @author Administrator
 */

//SOLR es NOSQL

@ManagedBean
@Named(value = "searchBean")
@Dependent
public class searchBean {

    String urlString = "http://localhost:8983/solr/#/PregunTIC";
    SolrClient solr = new HttpSolrClient.Builder(urlString).build();
    /**
     * Creates a new instance of searchBean
     */
    public searchBean() {
    }
    
    SolrQuery query = new SolrQuery().setQuery("Aqui no se que va");
    
    QueryResponse response = solr.query(query);
    
    SolrDocumentList list = response.getResults();
    
    SolrInputDocument document = new SolrInputDocument();
    //document.addField("id", "552199");
    //document.addField("name", "Gouda cheese wheel");
    //document.addField("price", "49.99");
    //UpdateResponse response = solr.add(document);

// Remember to commit your changes!

    solr.commit();
    
    solr.setRequestWriter(new BinaryRequestWriter());
}
